@extends('layouts.app')

@section('title', config('app.name', 'Laravel') . ' | Parks')

@section('content')
    <admin-parks-content></admin-parks-content>
@endsection