@extends('layouts.app')

@section('title', config('app.name', 'Laravel') . ' | Edit car')

@section('content')
    <admin-edit-car-form
        car-json="{{ $car }}"
        users-json="{{ $users }}"
        parks-json="{{ $parks }}">
    </admin-edit-car-form>
@endsection
