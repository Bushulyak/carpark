@extends('layouts.app')

@section('title', config('app.name', 'Laravel') . ' | Create user')

@section('content')
    <admin-create-user-form
        cars-json="{{ $cars }}">
    </admin-create-user-form>
@endsection
