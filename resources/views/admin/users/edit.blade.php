@extends('layouts.app')

@section('title', config('app.name', 'Laravel') . ' | Edit user')

@section('content')
    <admin-edit-user-form
        user-json="{{ $user }}"
        cars-json="{{ $cars }}">
    </admin-edit-user-form>
@endsection
