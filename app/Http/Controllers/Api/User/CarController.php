<?php

namespace App\Http\Controllers\Api\User;

use App\Car;
use App\Http\Resources\User\CarResource;
use App\Http\Requests\User\StoreCar;
use App\Http\Requests\User\UpdateCar;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CarController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cars = Car::whereHas('users', function (Builder $query) {
            $query->where('users.id', Auth::id());
        })->paginate(5);

        return CarResource::collection($cars);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCar $request)
    {
        try {
            $car = Car::create($request->validated());

            $car->users()->attach(Auth::id());
            $car->parks()->attach($this->explode(',', $request['parks_selected']));

            return new CarResource($car);

        } catch (\Exception $exception) {
            throw new HttpException(400, 'Invalid data');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCar $request, Car $car)
    {
        try {
            $request = $request->validated();

            $car->car_number = $request['car_number'];
            $car->save();

            $this->syncOrDetach($car, 'parks', $request['parks_selected']);
            $car->users()->syncWithoutDetaching(Auth::id());

            return new CarResource($car);

        } catch (\Exception $e) {
            throw new HttpException(400, 'Invalid data');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Car $car)
    {
        return redirect()->route('index');
    }
}
