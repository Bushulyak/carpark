<?php

namespace App\Http\Controllers\Api\Admin;

use App\Park;
use App\Http\Resources\Admin\ParkResource;
use App\Http\Requests\Admin\StorePark;
use App\Http\Requests\Admin\UpdatePark;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ParkController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ParkResource::collection(Park::orderBy('id', 'DESC')->paginate(5));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePark $request)
    {
        try {
            $park = Park::create($request->validated());

            $park->cars()->attach($this->explode(',', $request['cars_selected']));

            return new ParkResource($park);

        } catch (\Exception $exception) {
            throw new HttpException(400, 'Invalid data');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Park $park)
    {
        return new ParkResource($park);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePark $request, Park $park)
    {
        try {
            $request = $request->validated();

            $park->name = $request['name'];
            $park->address = $request['address'];
            $park->schedule = $request['schedule'];

            $park->save();

            $this->syncOrDetach($park, 'cars', $request['cars_selected']);

            return new ParkResource($park);

        } catch (\Exception $e) {
            throw new HttpException(400, 'Invalid data');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Park $park)
    {
        $park->cars()->detach();

        $park->delete();

        return response()->json(null, 204);
    }
}
