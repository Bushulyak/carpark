<?php

namespace App\Http\Controllers\Api\Admin;

use App\User;
use App\Http\Resources\Admin\UserResource;
use App\Http\Requests\Admin\StoreUser;
use App\Http\Requests\Admin\UpdateUser;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpKernel\Exception\HttpException;

class UserController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return UserResource::collection(User::orderBy('id', 'DESC')->paginate(5));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUser $request)
    {
        try {
            $request = $request->validated();

            $request['password'] = Hash::make($request['password']);

            $user = User::create($request);

            $user->cars()->attach($this->explode(',', $request['cars_selected']));

            return new UserResource($user);

        } catch (\Exception $exception) {
            throw new HttpException(400, 'Invalid data');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return new UserResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUser $request, User $user)
    {
        try {
            $request = $request->validated();

            $user->name = $request['name'];
            $user->email = $request['email'];
            $user->is_admin = $request['is_admin'];

            if (isset($request['password'])) {
                $user->password = Hash::make($request['password']);
            }

            $user->save();

            $this->syncOrDetach($user, 'cars', $request['cars_selected']);

            return new UserResource($user);

        } catch (\Exception $e) {
            throw new HttpException(400, 'Invalid data');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->cars()->detach();

        $user->delete();

        return response()->json(null, 204);
    }
}
