<?php

namespace App\Http\Controllers\Api\Admin;

use App\Car;
use App\Http\Resources\Admin\CarResource;
use App\Http\Requests\Admin\StoreCar;
use App\Http\Requests\Admin\UpdateCar;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CarController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return CarResource::collection(Car::orderBy('id', 'DESC')->paginate(5));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCar $request)
    {
        try {
            $car = Car::create($request->validated());

            $car->users()->attach($this->explode(',', $request['users_selected']));
            $car->parks()->attach($this->explode(',', $request['parks_selected']));

            return new CarResource($car);

        } catch (\Exception $exception) {
            throw new HttpException(400, 'Invalid data');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Car $car)
    {
        return new CarResource($car);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCar $request, Car $car)
    {
        try {
            $request = $request->validated();

            $car->car_number = $request['car_number'];
            $car->save();

            $this->syncOrDetach($car, 'parks', $request['parks_selected']);
            $this->syncWithoutDetaching(Auth::id());

            return new CarResource($car);

        } catch (\Exception $e) {
            throw new HttpException(400, 'Invalid data');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Car $car)
    {
        $car->users()->detach();
        $car->parks()->detach();

        $car->delete();

        return response()->json(null, 204);
    }
}
