<?php

namespace App\Http\Requests\User;

use App\Car;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class StoreCar extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }
    /**
     * Get message if validation fails
     * 
     * @return array
    */
    public function messages()
    {
        return [
            'car_number.required' => 'Введите, пожалуйста, номер автомобиля',
            'car_number.unique' => 'Данный номер автомобиля уже зарегистрирован',
            'car_number.min' => 'Номер автомобиля не может содержать менее 6 символов',
            'car_number.max' => 'Номер автомобиля не может содержать более 8 символов',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'car_number' => ['bail', 'required', 'unique:cars', 'min:6', 'max:8'],
            'parks_selected' => ['nullable'],
        ];
    }
}
