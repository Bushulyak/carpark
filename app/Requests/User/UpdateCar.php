<?php

namespace App\Http\Requests\User;

use App\Car;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Http\FormRequest;

class UpdateCar extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check()) {
            return Car::where('id', $this->route('car')->id)
                ->whereHas('users', function (Builder $query) {
                    $query->where('users.id', Auth::id());
                })
                ->exists();
        }

        return false;
    }

    public function messages()
    {
        return [
            'car_number.required' => 'Введите, пожалуйста, номер автомобиля',
            'car_number.unique' => 'Данный номер автомобиля уже зарегистрирован',
            'car_number.min' => 'Номер автомобиля не может содержать меньше 6 символов',
            'car_number.max' => 'Номер автомобиля не может содержать больше 8 символов',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'car_number' => ['bail', 'required', 'unique:cars,car_number,'.$this->car->id, 'min:6', 'max:8'],
            'parks_selected' => ['nullable'],
        ];
    }
}
