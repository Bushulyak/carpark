<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(App\Car::class, function (Faker $faker) {
    return [
        'car_number' => chr(rand(65,90))
            .chr(rand(65,90))
            .$faker->randomNumber(4)
            .chr(rand(65,90))
            .chr(rand(65,90)),
        'created_at' => $faker->dateTime(),
        'updated_at' => $faker->dateTime(),
    ];
});
