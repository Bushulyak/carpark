<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(App\Park::class, function (Faker $faker) {
    return [
        'name'  => $faker->company,
        'address' => $faker->address,
        'schedule' => $faker->dayOfWeek('saturday').'-'
            .$faker->dayOfWeek('saturday').', '
            .$faker->time('H:i', $max = '23:59').'-'
            .$faker->time('H:i', $max = '23:59'),
        'created_at' => $faker->dateTime(),
        'updated_at' => $faker->dateTime(),
    ];
});
